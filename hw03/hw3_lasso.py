import numpy as np
import numpy.random as rn
import numpy.linalg as la
import matplotlib.pyplot as plt

def frank_wolfe(x, A, b, t, gam):
    # Compute step size
    eta = 2.0/(t+2.0)

    # Compute y
    z = b-np.dot(A, x)
    # Find index that maximizes gradient
    i = np.argmax(np.abs(np.dot(z.T, A)))

    # Update x
    x *= (1-eta)
    x[i] += eta*gam*np.sign(np.dot(z.T, A[:, i]))
    return x


def subgradient(x, A, b, t, lam, c=1.0e-4):
    # Compute step size
    eta = c/np.sqrt(t+1.0)

    # Compute subgradient vector for l1 norm
    s = np.sign(x)

    # Compute subgradient for function
    g = np.dot(A.T, np.dot(A, x)-b) + lam*s

    # Update x
    x -= eta*g;
    return x


def descent(update, A, b, reg, T=int(1e4)):
    x = np.zeros(A.shape[1])
    error = []
    l1 = []
    for t in xrange(T):
        # update A (either subgradient or frank-wolfe)
        x = update(x, A, b, t, reg)
        
        # record error and l1 norm
        if (t % 1 == 0) or (t == T - 1):
            error.append(la.norm(np.dot(A, x) - b))
            l1.append(np.sum(abs(x)))

            assert not np.isnan(error[-1])

    return x, error, l1


def main(T=int(1e4)):
    A = np.load("A.npy")
    b = np.load("b.npy")

    # modify regularization parameters below
    x_sg, error_sg, l1_sg = descent(subgradient, A, b, reg=0.1, T=T)
    x_fw, error_fw, l1_fw = descent(frank_wolfe, A, b, reg=np.max(l1_sg), T=T)
    # add BTLS experiments

    # add plots for BTLS
    plt.clf()
    plt.plot(error_sg, label='Subgradient')
    plt.plot(error_fw, label='Frank-Wolfe')
    plt.title('Error')
    plt.legend()
    plt.savefig('error.eps')
    plt.clf()
    plt.plot(l1_sg, label='Subgradient')
    plt.plot(l1_fw, label='Frank-Wolfe')
    plt.title("$\ell^1$ Norm")
    plt.legend()
    plt.savefig('l1.eps')


if __name__ == "__main__":
    main()
