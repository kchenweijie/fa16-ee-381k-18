import numpy as np
import numpy.random as rn
import numpy.linalg as la
import matplotlib.pyplot as plt

def gradient_descent(b, X, y, t, reg, mu=1.0e-4):
    # Standard gradient descent
    eta = 1.0e-4


    return b


def nesterov(b, X, y, t, reg):
    # Nesterov's accelerated gradient descent
    return b


def gradient(b, X, y, mu):
    # Gradient of log liklihood
    
    return b


def descent(update, X, y, reg, T=int(1e4)):
    b = np.zeros(X.shape[1])
    error = []
    l1 = []
    for t in xrange(T):
        # update A (either subgradient or frank-wolfe)
        b = update(b, X, y, t, reg)
        
        # record error and l1 norm
        if (t % 1 == 0) or (t == T - 1):
            error.append(la.norm(np.dot(X, b) - y))
            l1.append(np.sum(abs(b)))

            assert not np.isnan(error[-1])

        if (0==t%100):
            print(t)

    return b, error, l1


def main(T=int(1e2)):
    X = np.loadtxt(open("X_test.csv","rb"),delimiter=",")[:,1:]
    y = np.loadtxt(open("y_test.csv","rb"),delimiter=",")

    print(X.shape)
    print(y.shape)

    # modify regularization parameters below
    x_gd, error_gd, l1_gd = descent(gradient_descent, X, y, reg=1.0e-4, T=T)
    x_nagd, error_nagd, l1_nagd = descent(nesterov, X, y, reg=1.0e-4, T=T)

    plt.clf()

    plt.figure(1)
    plt.plot(error_gd, label='Standard')
    plt.plot(error_nagd, label='Accelerated')
    plt.title('Error')
    plt.xlabel('Iteration')
    plt.ylabel(r'$\|X\beta-y\|$')
    plt.legend()

    plt.figure(2)
    plt.plot(l1_gd, label='Standard')
    plt.plot(l1_nagd, label='Accelerated')
    plt.title("$\ell^1$ Norm")
    plt.xlabel('Iteration')
    plt.ylabel(r'$\|\beta\|_1$')
    plt.legend()

    plt.show()


if __name__ == "__main__":
    main()
