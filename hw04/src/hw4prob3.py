import numpy as np
import numpy.random as rn
import numpy.linalg as la
import matplotlib.pyplot as plt

def pgd(b, X, y, t):
    # Projected gradient descent
    eta = 1.0e-4/np.sqrt(t+1.0)

    # Update beta
    b = b-eta*np.dot(X.T, l1_subgrad(np.dot(X,b)-y))

    # Project beta back onto the probability simplex
    b = projX(b)

    return b


def mirror_descent(b, X, y, t):
    # Mirror descent
    eta = 1.0e-2/np.sqrt(t+1.0)

    # Initialize unprojected update
    u = b

    # Obtain l1 subgradient
    subgrad_b = np.dot(X.T, l1_subgrad(np.dot(X,b)-y))

    # Compute unprojected update
    for i in range(len(b)):
        u[i] = b[i]*np.exp(-eta*subgrad_b[i])

    # If beta is all zeros, give it some numbers to work with
    if (0==sum(u)):
        u = rn.rand(len(b))

    # Project back onto probability simplex
    b = u/sum(abs(u))

    return b


def projX(b):
    # Projection onto probability simplex
    # Algorithm given by "Projection onto the probability simplex..." by Wang et al
    u = sorted(b, reverse=True)

    idx = len(b)-1
    found = False
    while (0<=idx) and not found:
        if (0 >= u[idx]+(1.0-sum(u[0:idx+1]))/(idx+1.0)):
            idx -= 1
        else:
            found = True

    lam = (1.0-sum(u[0:idx+1]))/(idx+1.0)

    for i in range(len(b)):
        b[i] = max(b[i]+lam, 0)

    return b


def l1_subgrad(b):
    # Subgradient of the l1 norm
    return np.sign(b)


def descent(update, X, y, T=int(1e4)):
    b = np.zeros(X.shape[1])
    l1 = []
    for t in xrange(T):
        # Update beta
        b = update(b, X, y, t)

        # Record error and l1 norm
        if (t % 1 == 0) or (t == T-1):
            l1.append(np.sum(abs(np.dot(X,b)-y)))
    return b, l1


def main(T=int(1e4)):
    X = np.load("X.npy")
    y = np.load("y.npy")

    b_pgd, l1_pgd = descent(pgd, X, y, T=T)
    b_md, l1_md = descent(mirror_descent, X, y, T=T)

    plt.clf()

    plt.figure(1)
    plt.plot(l1_pgd, label='PGD')
    plt.plot(l1_md, label='Mirror')
    plt.title('$\ell^1$ Norm')
    plt.xlabel('Iteration')
    plt.ylabel(r'$\|{X\beta-y}\|_1$')
    plt.legend()

    plt.figure(2)
    plt.plot(b_pgd, label='PGD')
    plt.plot(b_md, label='Mirror')
    plt.title(r'$\beta$')
    plt.xlabel('Index')
    plt.ylabel(r'$\beta_i$')
    plt.legend()

    plt.show()
    

if __name__=="__main__":
    main()
