import numpy as np
import numpy.random as rn
import numpy.linalg as la
import matplotlib.pyplot as plt

def psgd(X, M, O, t, reg):
    # Projected subgradient descent
    
    # Compute eta
    eta = reg(t)

    # Update X
    X -= eta*subgradient(X)

    # Project X back onto the matrix space
    X = project(X, M, O)

    return X


def subgradient(X):
    # Gradient of nuclear norm

    # Execute singular value decomposition
    U, S, V = la.svd(X)

    # Return gradient
    return np.dot(U, V.T)


def project(X, M, O):
    # Project back onto matrix space
    X[1==O] = M[1==O]
    return X


def step_inv(t):
    return 1.0/(t+1.0)


def step_sqrt(t):
    return 1.0/np.sqrt(t+1.0)


def descent(update, M, O, reg, T=int(1e4)):
    X = np.zeros(M.shape)
    X = project(X, M, O)
    error = []
    rankX = []
    for t in xrange(T):
        # update A (either subgradient or frank-wolfe)
        X = update(X, M, O, t, reg)
        
        # record error and l1 norm
        if (t % 1 == 0) or (t == T - 1):
            error.append(1e-4*la.norm(M-X)**2)
            rankX.append(la.matrix_rank(X))

            assert not np.isnan(error[-1])

    return X, error, rankX


def main(T=int(1e4)):
    M = np.loadtxt(open("M.csv","rb"),delimiter=",")
    O = np.loadtxt(open("O.csv","rb"),delimiter=",")

    # modify regularization parameters below
    x_inv, error_inv, rankX_inv = descent(psgd, M, O, reg=step_inv, T=T)
    x_sqrt, error_sqrt, rankX_sqrt = descent(psgd, M, O, reg=step_sqrt, T=T)

    plt.clf()

    plt.figure(1)
    plt.plot(error_inv, label=r'$\eta=1/k$')
    plt.plot(error_sqrt, label=r'$\eta=1/\sqrt{k}$')
    plt.title('Error')
    plt.xlabel('Iteration')
    plt.ylabel(r'$\|M-X_k\|^2_F$')
    plt.legend()

    plt.figure(2)
    plt.plot(rankX_inv, label=r'$\eta=1/k$')
    plt.plot(rankX_sqrt, label=r'$\eta=1/\sqrt{k}$')
    plt.title('Rank')
    plt.xlabel('Iteration')
    plt.ylabel('rank(X)')
    plt.legend()

    plt.show()


if __name__ == "__main__":
    main()
