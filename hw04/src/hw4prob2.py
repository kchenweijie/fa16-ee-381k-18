import numpy as np
import numpy.random as rn
import numpy.linalg as la
import matplotlib.pyplot as plt

def frank_wolfe(x, A, b, t, gam):
    # Compute step size
    eta = 2.0/(t+2.0)

    # Compute y
    z = b-np.dot(A, x)
    # Find index that maximizes gradient
    i = np.argmax(np.abs(np.dot(z.T, A)))

    # Update x
    x *= (1-eta)
    x[i] += eta*gam*np.sign(np.dot(z.T, A[:, i]))

    return x


def subgradient(x, A, b, t, lam, c=1.0e-4):
    # Compute step size
    eta = c/np.sqrt(t+1.0)

    # Compute subgradient vector for l1 norm
    s = np.sign(x)

    # Compute subgradient for function
    g = np.dot(A.T, np.dot(A, x)-b) + lam*s

    # Update x
    x -= eta*g;

    return x


def ista(x, A, b, t, alpha, eta=1.0e-4):
    # Take a step
    x -= 2*eta*np.dot(A.T, np.dot(A,x)-b)

    # Apply the shrinkage operator
    for i in range(len(x)):
        x[i] = (abs(x[i])-alpha)*np.sign(x[i])

    return x


def fista(x, A, b, t, alpha, eta=1.0e-4):
    # Take a step using ISTA
    y = ista(x, A, b, t, alpha, eta)

    # Compute lambda
    # In know this is computationally inefficient but I'm too lazy to restructure the code
    lam = 0.0
    for i in range(t):
        lam = (1.0+np.sqrt(1.0+4.0*lam**2.0))/2.0

    lam_plus = (1.0+np.sqrt(1.0+4.0*lam**2.0))/2.0

    # Compute gamma
    gam = (1-lam)/(lam_plus)

    # Update x
    x = (1-gam)*y + gam*x

    return x


def descent(update, A, b, reg, T=int(1e4)):
    x = np.zeros(A.shape[1])
    error = []
    l1 = []
    for t in xrange(T):
        # update A (either subgradient or frank-wolfe)
        x = update(x, A, b, t, reg)
        
        # record error and l1 norm
        if (t % 1 == 0) or (t == T - 1):
            error.append(la.norm(np.dot(A, x) - b))
            l1.append(np.sum(abs(x)))

            assert not np.isnan(error[-1])

    return x, error, l1


def main(T=int(1e4)):
    A = np.load("A_train.npy")
    b = np.load("b_train.npy")

    # modify regularization parameters below
    x_sg, error_sg, l1_sg = descent(subgradient, A, b, reg=1.0e-4, T=T)
    x_fw, error_fw, l1_fw = descent(frank_wolfe, A, b, reg=np.max(l1_sg), T=T)
    x_ista, error_ista, l1_ista = descent(ista, A, b, reg=1.0e-4, T=T)
    x_fista, error_fista, l1_fista = descent(fista, A, b, reg=1.0e-4, T=T)

    plt.clf()

    plt.figure(1)
    plt.plot(error_sg, label='Subgradient')
    plt.plot(error_fw, label='Frank-Wolfe')
    plt.plot(error_ista, label='ISTA')
    plt.plot(error_fista, label='FISTA')
    plt.title('Error - Train')
    plt.xlabel('Iteration')
    plt.ylabel(r'$\|Ax-b\|$')
    plt.legend()

    plt.figure(2)
    plt.plot(l1_sg, label='Subgradient')
    plt.plot(l1_fw, label='Frank-Wolfe')
    plt.plot(l1_ista, label='ISTA')
    plt.plot(l1_fista, label='FISTA')
    plt.title("$\ell^1$ Norm - Train")
    plt.xlabel('Iteration')
    plt.ylabel(r'$\|x\|_1$')
    plt.legend()

    plt.show()


if __name__ == "__main__":
    main()
